import gsap, { Power2 } from 'gsap';

export class RotationController {
  constructor(data) {
    this.wheelRotateDegrees = 40;
    this.wheelRotateCount = 6;

    this.itemRotateDegrees = 40;
    this.itemRotateCount = 6;

    this.itemsData = data;

    this.wheelItems = [];
    this.isAnimating = false;
  }

  rotateSlideItem() {
    const items = Array.from(document.querySelectorAll('.home-screen__item'));

    this.itemRotateCount--;

    gsap.set('#item' + this.itemRotateCount, {
      rotation: -(3 * this.itemRotateDegrees),
    });

    const activeItem = document.querySelector('.home-screen__item--active');

    gsap.to(activeItem, 0.6, {
      opacity: 0,
      ease: Power2.easeInOut,
    });

    if (activeItem.previousElementSibling) {
      gsap.to([activeItem.previousElementSibling], 1.2, {
        opacity: 1,
        ease: Power2.easeInOut,
      });
    } else {
      gsap.to([items[items.length - 1]], 1.2, {
        opacity: 1,
        ease: Power2.easeInOut,
      });
    }

    const cls = 'home-screen__item--active';
    let nextItem;

    if (activeItem.previousElementSibling) {
      activeItem.previousElementSibling.classList.add(cls);
      nextItem = activeItem.previousElementSibling;
    } else {
      nextItem = items[items.length - 1];
      nextItem.classList.add(cls);
    }

    const slideIndex = items.findIndex((item) => item === nextItem);
    const { article, defaultColorLabel, colors } = this.itemsData[slideIndex];
    const color = colors[defaultColorLabel];

    gsap.to('.home-screen__item', 1.2, {
      rotation: `+=${this.itemRotateDegrees}`,
      ease: Power2.easeInOut,
      onStart: () => {
        const changeEvent = new CustomEvent('slidechangestart', {
          detail: { slideIndex, article, color },
        });
        document.dispatchEvent(changeEvent);
      },
      onComplete: () => {
        activeItem.classList.remove(cls);

        const changeEvent = new CustomEvent('slidechangeend', {
          detail: { slideIndex, article, color },
        });
        document.dispatchEvent(changeEvent);
      },
    });

    if (this.itemRotateCount === 1) {
      this.itemRotateCount = 6;
    }
  }

  rotateWheel(event) {
    const target = event.target.classList.contains('wheel__name')
      ? event.target.parentNode
      : event.target;

    if (this.isAnimating) {
      return;
    }

    if (target.classList.contains('wheel__item--active')) {
      return;
    }

    const dir = target.classList.contains('wheel__item--next')
      ? 'next'
      : 'prev';

    this.isAnimating = true;

    this.toggleStateClassNames(target, dir);

    if (dir === 'prev') {
      this.wheelRotateCount--;

      if (this.wheelRotateCount === 1) {
        this.wheelRotateCount = 5;
      }

      this.rotateSlideItem();

      gsap.set('#box' + this.wheelRotateCount, {
        rotation: -(3 * this.wheelRotateDegrees),
      });
      gsap.to('.wheel__item', 1.6, {
        rotation: '+=' + this.wheelRotateDegrees,
        ease: Power2.easeInOut,
        onComplete: () => (this.isAnimating = false),
      });
    } else {
      this.wheelRotateCount++;

      if (this.wheelRotateCount >= 6) {
        this.wheelRotateCount = 1;
      }

      this.rotateSlideItem();

      gsap.set('#box' + this.wheelRotateCount, {
        rotation: 3 * this.wheelRotateDegrees,
      });
      gsap.to('.wheel__item', 1.6, {
        rotation: '-=' + this.wheelRotateDegrees,
        ease: Power2.easeInOut,
        onComplete: () => (this.isAnimating = false),
      });
    }
  }

  toggleStateClassNames(target, dir) {
    target.classList.add('wheel__item--active');

    document
      .querySelector('.wheel__item--next')
      .classList.remove('wheel__item--next');
    document
      .querySelector('.wheel__item--prev')
      .classList.remove('wheel__item--prev');

    if (dir === 'prev') {
      if (target.nextElementSibling) {
        target.nextElementSibling.classList.remove('wheel__item--active');
        target.nextElementSibling.classList.add('wheel__item--next');

        if (target.previousElementSibling) {
          target.previousElementSibling.classList.add('wheel__item--prev');
        } else {
          this.wheelItems[this.wheelItems.length - 1].classList.add(
            'wheel__item--prev',
          );
        }
      } else {
        this.wheelItems[0].classList.remove('wheel__item--active');
        this.wheelItems[0].classList.add('wheel__item--next');

        if (this.wheelItems[0].previousElementSibling) {
          this.wheelItems[0].previousElementSibling.classList.add(
            'wheel__item--prev',
          );
        } else {
          this.wheelItems[this.wheelItems.length - 1].classList.add(
            'wheel__item--prev',
          );
        }
      }
    } else {
      if (target.previousElementSibling) {
        target.previousElementSibling.classList.remove('wheel__item--active');
        target.previousElementSibling.classList.add('wheel__item--prev');

        if (target.nextElementSibling) {
          target.nextElementSibling.classList.add('wheel__item--next');
        } else {
          this.wheelItems[0].classList.add('wheel__item--next');
        }
      } else {
        this.wheelItems[this.wheelItems.length - 1].classList.remove(
          'wheel__item--active',
        );
        this.wheelItems[this.wheelItems.length - 1].classList.add(
          'wheel__item--prev',
        );

        if (this.wheelItems[this.wheelItems.length - 1].nextElementSibling) {
          this.wheelItems[
            this.wheelItems.length - 1
          ].nextElementSibling.classList.add('wheel__item--next');
        } else {
          this.wheelItems[0].nextElementSibling.classList.add(
            'wheel__item--next',
          );
        }
      }
    }
  }

  initialize() {
    gsap.set('.wheel__item', { transformOrigin: '65px 380px' });
    gsap.set('.home-screen__item:not(.home-screen__item--active)', {
      opacity: 0,
    });

    // NOTE: don't do that in production
    gsap.set('#item1', { rotation: -(2 * this.itemRotateDegrees) });
    gsap.set('#item2', { rotation: -this.itemRotateDegrees });
    gsap.set('#item4', { rotation: this.itemRotateDegrees });
    gsap.set('#item5', { rotation: this.itemRotateDegrees * 2 });

    gsap.set('#box1', { rotation: -(2 * this.wheelRotateDegrees) });
    gsap.set('#box2', { rotation: -this.wheelRotateDegrees });
    gsap.set('#box4', { rotation: this.wheelRotateDegrees });
    gsap.set('#box5', { rotation: this.wheelRotateDegrees * 2 });

    this.wheelItems = Array.from(document.querySelectorAll('.wheel__item'));
    this.wheelItems.forEach((item) =>
      item.addEventListener('click', this.rotateWheel.bind(this)),
    );
  }
}
