import gsap, { Expo } from 'gsap';

export class RevealController {
  constructor() {
    this.header = document.querySelector('.header');
    this.content = document.querySelector('.home-screen__content');
    this.colorPicker = document.querySelector('.color-picker--initial');
    this.wheel = document.querySelector('.wheel');
    this.meta = document.querySelector('.home-screen__meta-data');
  }

  initialize() {
    const tl = gsap.timeline();

    tl.to(this.header, 1.5, { y: 0, ease: Expo.easeOut });
    tl.to(this.content, 1, { opacity: 1 }, '-=1.2');
    tl.to(
      this.colorPicker,
      {
        opacity: 1,
        ease: 'none',
        onComplete: () => {
          this.colorPicker.classList.remove('color-picker--initial');
        },
      },
      '-=1',
    );
    tl.to([this.wheel, this.meta], 1.5, { y: 0, ease: Expo.easeOut }, '-=1');
  }
}
