import gsap, { Power2 } from 'gsap';

export class ProductController {
  constructor() {
    this.colorPickerList = Array.from(
      document.querySelectorAll('.color-picker'),
    );

    this.imageItems = Array.from(
      document.querySelectorAll('.home-screen__item'),
    );

    this.activeColorPicker = this.colorPickerList.find((cpl) =>
      cpl.classList.contains('color-picker--active'),
    );

    this.articlePrev = document.querySelector('.home-screen__article');
    this.articleNext = document.querySelector('.home-screen__article--next');
    this.colorPrev = document.querySelector('.home-screen__color');
    this.colorNext = document.querySelector('.home-screen__color--next');

    this.activeBG = document.querySelector('.home-screen__bg--active');

    document.addEventListener(
      'slidechangestart',
      this.changeHomeScreenBg.bind(this),
    );
    document.addEventListener(
      'slidechangeend',
      this.updateActiveItemData.bind(this),
    );

    this.onColorItemClick = this.onColorItemClick.bind(this);
  }

  changeHomeScreenBg(event) {
    const { color } = event.detail;
    const cls = `.home-screen__bg--${color.label}`;

    const nextBG = document.querySelector(cls);

    if (!this.activeBG.classList.contains(cls)) {
      gsap.to(this.activeBG, 1, {
        opacity: 0,
        onComplete: () =>
          this.activeBG.classList.remove('home-screen__bg--active'),
      });

      gsap.to(nextBG, 1, {
        opacity: 1,
      });
    }
  }

  updateActiveItemData(event) {
    const { slideIndex, article, color } = event.detail;
    this.changeActiveColorPicker(slideIndex);
    this.changeMetaData({ article, color });
  }

  changeMetaData({ article, color }) {
    gsap.set('.home-screen__article, .home-screen__color', { y: 0 });

    this.articlePrev.textContent = this.articleNext.textContent;
    this.articleNext.textContent = article;
    this.colorPrev.textContent = this.colorNext.textContent;
    this.colorNext.textContent = color.label;

    gsap.to('.home-screen__article, .home-screen__color', 1, {
      y: '-=24',
      ease: Power2.easeInOut,
    });
  }

  changeActiveColorPicker(nextItemIndex) {
    const nextColorPicker = this.colorPickerList[nextItemIndex];

    gsap.to(nextColorPicker, 0.5, {
      opacity: 1,
      y: '-=30',
      ease: Power2.easeInOut,
      onComplete: () => nextColorPicker.classList.add('color-picker--active'),
    });

    gsap.to(this.activeColorPicker, 0.5, {
      opacity: 0,
      y: '+=30',
      ease: Power2.easeInOut,
      onComplete: () => {
        this.activeColorPicker.classList.remove('color-picker--active');
        this.activeColorPicker = nextColorPicker;
      },
    });
  }

  onColorItemClick(event) {
    const { target } = event;
    const activeColorItem = target.parentNode.querySelector(
      '.color-picker__item--active',
    );

    if (target === activeColorItem) {
      return;
    }

    target.classList.add('color-picker__item--active');
    activeColorItem.classList.remove('color-picker__item--active');

    const color = { label: target.dataset.colorLabel };
    this.changeHomeScreenBg({ detail: { color } });
    this.changeColorRelatedImage(color.label);
  }

  changeColorRelatedImage(colorLabel) {
    const activeImageItem = this.imageItems.find((item) =>
      item.classList.contains('home-screen__item--active'),
    );
    const activeColorImage = activeImageItem.querySelector(
      '.home-screen__img--active',
    );
    const nextColorImage = activeImageItem.querySelector(
      `[data-color-variant="${colorLabel}"]`,
    );

    const tl = gsap.timeline({
      onComplete: () => {
        activeColorImage.classList.remove('home-screen__img--active');
        nextColorImage.classList.add('home-screen__img--active');
      },
    });
    tl.to(activeColorImage, { opacity: 0 });
    tl.to(nextColorImage, { opacity: 1 }, '-=0.8');
  }

  initialize() {
    this.colorPickerList.forEach((cpl) => {
      const colorItems = Array.from(
        cpl.querySelectorAll('.color-picker__item'),
      );
      colorItems.forEach((item) =>
        item.addEventListener('click', this.onColorItemClick),
      );
    });
  }
}
