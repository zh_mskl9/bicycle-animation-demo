/**
 * This file is just a silly example to show everything working in the browser.
 * When you're ready to start on your site, clear the file. Happy hacking!
 **/

import { RevealController } from './reveal.controller';
import { RotationController } from './rotation.controller';
import { ProductController } from './product.controller';

const itemsData = [
  {
    article: '0012253',
    defaultColorLabel: 'red',
    colors: {
      red: {
        background:
          'radial-gradient(50% 50% at 50% 50%, #FFFAFA 0%, #FFD8D8 100%)',
        label: 'red',
      },
    },
  },
  {
    article: '0012254',
    defaultColorLabel: 'black',
    colors: {
      black: {
        background:
          'radial-gradient(50% 50% at 50% 50%, #FFFAFA 0%, #BBBBBB 100%)',
        label: 'black',
      },
    },
  },
  {
    article: '0012255',
    defaultColorLabel: 'orange',
    colors: {
      orange: {
        background:
          'radial-gradient(50% 50% at 50% 50%, #FFFAFA 0%, #FFEFD8 100%)',
        label: 'orange',
      },
    },
  },
  {
    article: '0012256',
    defaultColorLabel: 'red',
    colors: {
      red: {
        background:
          'radial-gradient(50% 50% at 50% 50%, #FFFAFA 0%, #FFD8D8 100%)',
        label: 'red',
      },
    },
  },
  {
    article: '0012257',
    defaultColorLabel: 'red',
    colors: {
      red: {
        background:
          'radial-gradient(50% 50% at 50% 50%, #FFFAFA 0%, #FFD8D8 100%)',
        label: 'red',
      },
    },
  },
];

const revealController = new RevealController(itemsData);
revealController.initialize();

const rotationController = new RotationController(itemsData);
rotationController.initialize();

const productController = new ProductController();
productController.initialize();
